package Controllers;

import Views.FenetrePrincipale;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuController implements ActionListener {

    private FenetrePrincipale fenetre;

    public MenuController(FenetrePrincipale fenetre) {
        this.fenetre = fenetre;
    }

    /**
     * Adapte la réaction selon le bouton cliqué dans le menu
     * @param actionEvent on vérifie sa source pour savoir quel bouton a été pressé
     */
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == fenetre.fermerItem) {
            System.exit(0);
        }

        if (actionEvent.getSource() == fenetre.reglesItem) {
            JOptionPane.showMessageDialog(null, fenetre.reglesScrollPane, "Règles du jeu", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
