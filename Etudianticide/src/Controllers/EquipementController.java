package Controllers;

import Views.EquipementView;

import javax.swing.*;

public class EquipementController {

    private EquipementView view;

    public EquipementController(EquipementView equipementView) {

        view = equipementView;
    }

    /**
     * Traite la réponse de l'utilisateur quant à s'il veut utiliser un équipement
     * @param reponse réponse de l'utilisateur à la popup de choix
     */
    public void utilisationEquipement(int reponse) {

        if (reponse == JOptionPane.NO_OPTION) {
            System.out.println(JeuController.professeurCourant.getNom() + " a choisi de ne pas utiliser d'équipement");
            JeuController.professeurCourant.setEquipementCourant(null);

        } else if (reponse == JOptionPane.YES_OPTION) {
            System.out.println(JeuController.professeurCourant.getNom() + " a choisi d'utiliser un équipement");

            String[] equipements = JeuController.professeurCourant.getInventaire();
            this.view.choisirEquipement(equipements);

        } else if (reponse == JOptionPane.CLOSED_OPTION) {
            System.out.println("JOptionPane closed");
        }
    }

    /**
     * Effectue la mise à jour de l'équipement de professeur
     * suite au choix de l'utilisateur dans la vue
     * @param equipSelect équipement choisi par l'utilisateur
     */
    public void choixEquipement(Object equipSelect) {

        System.out.println("Equipement: " + equipSelect.toString());
        JeuController.professeurCourant.setEquipementCourant(equipSelect.toString());
    }
}
