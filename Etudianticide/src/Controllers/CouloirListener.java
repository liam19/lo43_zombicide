package Controllers;

import Views.CouloirPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CouloirListener implements MouseListener {

    private CouloirPanel couloir;

    public CouloirListener(CouloirPanel couloirPanel) {
        this.couloir = couloirPanel;
    }


    @Override
    public void mouseClicked(MouseEvent e) {    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println(couloir.getNom() + " " + couloir.getIndex());
        System.out.println("Index  du couloir parmi les zones : " + couloir.getIndex());

        JeuController.zoneCliquee = couloir.getIndex();

        couloir.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        couloir.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
