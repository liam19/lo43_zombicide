package Controllers;

import Views.SallePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SalleListener  implements MouseListener {

    private SallePanel salle;

    public SalleListener(SallePanel sallePanel) {
        this.salle = sallePanel;
    }

    /**
     * Modifie la valeur de la zone cliquée selon la salle que l'utilisateur a cliquée
     * @param mouseEvent clic de la souris
     */
    @Override
    public void mouseClicked(MouseEvent mouseEvent) {    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("Salle " + salle.getIndexSalle());
        System.out.println("Index  de la salle parmi les zones : " + salle.getIndexSalle());

        JeuController.zoneCliquee = salle.getIndexSalle();

        salle.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        salle.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

}
