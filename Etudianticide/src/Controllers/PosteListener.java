package Controllers;

import Views.PostePanel;
import Views.SallePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PosteListener implements MouseListener {

    private SallePanel salle;
    private PostePanel poste;

    public PosteListener(PostePanel postePanel, SallePanel sallePanel) {
        this.salle = sallePanel;
        this.poste = postePanel;
    }


    @Override
    public void mouseClicked(MouseEvent e) {    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println(poste.getNom() + " " + poste.getIndex());
        System.out.println("Index  du poste : " + poste.getIndex()
                + ", appartenant à la salle d'index : " + poste.getIndexSalle());

        JeuController.zoneCliquee = poste.getIndexSalle();
        JeuController.posteClique = poste.getIndex();

        salle.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
        poste.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        salle.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        poste.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
