package Controllers;

import Models.Jeu;
import Models.Professeur;
import Models.Salle;
import Views.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ActionController implements ActionListener {
    private ContenuGlobal contenu;
    private ProfesseurPanel professeurPanel;
    private EtudiantsPanel etudiantsPanel;
    private EquipementView equipementView = new EquipementView();

    //Permet de stocker l'action de déplacement choisie par l'utilisateur, afin d'effectuer les tests adéquats
    private String deplacementChoisi = "";


    public ActionController(ContenuGlobal contenuGlobal) {
        contenu = contenuGlobal;
        professeurPanel = contenu.getProfesseurPanel();
        etudiantsPanel = contenu.getEtudiantPanel();
    }

    /**
     * Démarre l'action du professeur selon la bouton radio sélectionné
     * ou confirme la lecture du log des etudiants
     * @param e le bouton de validation a été appuyé
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (JeuController.index != -1) { // c'est le tour des professeurs

            if (professeurPanel.isValidChoixActionPanel()) { // on choisit l'action à effectuer
                choixAction();
            } else { // c'est une action de déplacement à traiter
                if (!professeurPanel.getChoixDeplacementPanel().containsMessageDeplacemennt()) {
                    choixActionDeplacement();
                } else { // on traite le déplacement selon la zone choisie
                    boolean verifAction = false;
                    if (deplacementChoisi.equals("Changer de zone")) {
                        verifAction = effectuerDeplacement();
                    }
                    if (deplacementChoisi.equals("Installer un étudiant")) {
                        verifAction = effectuerInstallation();
                        professeurPanel.getChoixActionPanel().enableValiderUV();
                    }
                    if (verifAction) { //Si une des actions a été effectuée...
                        decompterAction();
                        deplacementChoisi = "";
                        professeurPanel.getChoixDeplacementPanel().removeMessageDeplacement();
                        professeurPanel.switchActionsDeplacement();
                    }
                    else {
                        System.out.println("Déplacement non-autorisé");
                        professeurPanel.getChoixDeplacementPanel().errorDeplacement();
                    }
                }
            }
        } else { // c'est le tour des étudiants
            JeuController.index = 0;
            JeuController.professeurCourant = Jeu.professeurs.get(JeuController.index);
            updateTextEtudiantLabel(); //On update le label concernant l'étudiant associé au prof courant
            installerAPoste(); //On met à jour l'affichage, si le prof courant est installer à un poste
            professeurPanel.getChoixDeplacementPanel().resetChoixDeplacementPanel(); //On réinitialise le panel ChoixDeplacementPanel

            contenu.getCardLayout().show(contenu.getCardPanel(), "professeur");
        }
        //professeurPanel.update();
        enableActions();
        enableActionsDeplacement();
        professeurPanel.update();

        JeuController.zoneCliquee = -1;
        JeuController.posteClique = -1;
    }

    /**
     * Permet de mettre à jour l'affichage, si le prof courant est installer à un poste
     */
    private void installerAPoste() {
        if (JeuController.professeurCourant.isInstallerAPoste())
            professeurPanel.getChoixActionPanel().enableValiderUV();

    }

    /**
     * Méthode permettant de mettre à jour le label concernant l'étudiant associé au prof, si il existe
     */
    private void updateTextEtudiantLabel() {
        if (JeuController.professeurCourant.aEtudiantAssocie()) {
            professeurPanel.ajoutNomEtudiantLabel();
        }
        else {
            professeurPanel.reinitialiserNomEtudiantLabel();
        }
    }

    /**
     * Méthode permettant de choisir parmi les actions possibles (sauf les actions de déplacements)
     */
    private void choixAction() {
        int i = 0;
        while (!professeurPanel.getChoixActionPanel().getListOptions().get(i).isSelected()) {
            ++i;
        }
        String choix = professeurPanel.getChoixActionPanel().getListOptions().get(i).getText();
        Professeur profCourant = JeuController.professeurCourant;
        System.out.println("Action sélectionnée : " + choix);
        switch (choix) {
            case "Ne rien faire":
                profCourant.neRienFaire();
                decompterAction();
                break;
            case "Valider UV":
                if (profCourant.getInventaire().length == 0) {
                    JeuController.professeurCourant.setEquipementCourant(null);
                    equipementView.pasEquipement();
                } else {
                    equipementView.utiliserEquipement(); //On demande à l'utilisateur si il souhaite sélectionner un équipement ou non
                }
                int valeurDe = JeuController.jeu.getDe().lancerDe(); //On lance le Dé
                    boolean uvValides = profCourant.validerUv(valeurDe); //On valider les UVs
                    String messageUVsValides; //String stockant un message à afficher aux joueurs
                    decompterAction();
                    if (uvValides) { //Si les UVs ont bien été validées
                        messageUVsValides = profCourant.getNom() + " a validé les UVs de son étudiant avec un lancé de dé de " + valeurDe;
                        professeurPanel.resetProfesseurPanel(); //On réinitialise ProfesseurPanel
                        professeurPanel.reinitialiserNomEtudiantLabel(); //On réinitialise le label de l'étudiant associé au prof courant
                    }
                    else {
                        messageUVsValides = profCourant.getNom() + " n'a pas réussi à valider les UVs de avec un lancé de dé de " + valeurDe;
                    }
                professeurPanel.affichageLancerDe(messageUVsValides); //On affiche le message

                break;
            case "Trouver équipement":
                equipementView.trouverEquipement(JeuController.professeurCourant.trouverEquipement());
                decompterAction();
                break;
            case "Se déplacer":
                enableActionsDeplacement();
                professeurPanel.switchActionsDeplacement();
                break;
        }
    }

    /**
     * Méthode permettant d'activer les choix adéquats
     */
    private void enableActions() {
        ChoixActionPanel choixActionPanel = professeurPanel.getChoixActionPanel();
        Professeur prof = JeuController.professeurCourant;

        if (prof.isInstallerAPoste()) {
            choixActionPanel.enableValiderUV();
        }
        else {
            choixActionPanel.resetChoixActionPanel();
        }
    }

    /**
     * Méthode permettant d'activer les actions de déplacement adéquates
     */
    private void enableActionsDeplacement() {
        ArrayList<JRadioButton> listChoix = professeurPanel.getChoixDeplacementPanel().getListChoix();
        ChoixDeplacementPanel choixDeplacementPanel = professeurPanel.getChoixDeplacementPanel();
        String[] etudiants = JeuController.professeurCourant.recupererNomsEtudiants();
        Professeur prof = JeuController.professeurCourant;

        //Si il y a des étudiants dans la zone et que prof courant n'a pas d'étudiant associé
        if (etudiants.length != 0 && !prof.aEtudiantAssocie()) {
            choixDeplacementPanel.enableRencontrerEtudiant();
        }
        else { //Sinon, on désactive l'option "rencontrer un étudiant"
            choixDeplacementPanel.disableRecontrerEtudiant();
        }

        //Si la zone actuelle est une Salle et que le profCourant est lié à un étudiant, on peut effectuer l'action "installer un étudiant"
        if (prof.getZonePosition() instanceof Salle && prof.aEtudiantAssocie()) {
            choixDeplacementPanel.enableInstallerEtudiant();
            if (deplacementChoisi.equals("Installer un étudiant")) { //Dans ce cas, l'étudiant doit rester sur "installer un étudiant" tant qu'il n'a pas sélectionné de poste
                choixDeplacementPanel.disableChangerZone();
                choixDeplacementPanel.enableInstallerEtudiant();
            }
        }

    }

    /**
     * Méthode permettant de choisir une action parmi les actions spécifiques de déplacement
     */
    private void choixActionDeplacement() {
        int i = 0;
        ArrayList<JRadioButton> listChoix = professeurPanel.getChoixDeplacementPanel().getListChoix();
        while(!listChoix.get(i).isSelected()) {
            ++i;
        }
        deplacementChoisi = listChoix.get(i).getText(); //On stocke le choix du joueur dans notre attribut deplacementChoisi
        System.out.println("Action de déplacement sélectionnée : " + deplacementChoisi);

        switch (deplacementChoisi) {
            case "Changer de zone" :
                professeurPanel.getChoixDeplacementPanel().addMessage("Sélectionner une zone et la valider"); //On appelle addMessage de professeurPanel
                break;
            case "Rencontrer un étudiant" :
                Object etudiantSelect = professeurPanel.choisirEtudiant(JeuController.professeurCourant.recupererNomsEtudiants());
                if (etudiantSelect != null) {
                    System.out.println("Etudiant rencontré: " + etudiantSelect.toString());
                    JeuController.professeurCourant.rencontrerEtudiant(etudiantSelect.toString());
                    contenu.getPlateau().repaint();

                }
                deplacementChoisi = "";
                professeurPanel.switchActionsDeplacement();
                professeurPanel.ajoutNomEtudiantLabel();
                decompterAction();
                break;
            case "Installer un étudiant" :
                professeurPanel.getChoixDeplacementPanel().addMessage("Sélectionner un poste à occuper et valider");
                break;
        }
    }

    /**
     * Ajoute une action au compteur du professeur
     * Quand son nombre d'actions effectuées est égal au nombre d'actions possibles
     * on passe alors au professeur suivant et on réinitialise le compteur
     */
    private void decompterAction() {
        JeuController.professeurCourant.setCompteurActions(JeuController.professeurCourant.getCompteurActions()+1);
        nextProfesseur();

        System.out.println("Action du professeur courant : " + JeuController.professeurCourant.getCompteurActions());
    }

    /**
     * Une fois que le nombre d'actions autorisées du professeur courant est effectué
     * On passe au professeur suivant
     * Si on arrive à la fin de la liste, on passe au tour des étudiants
     */
    private void nextProfesseur() {
        if (JeuController.professeurCourant.getNbActions() == JeuController.professeurCourant.getCompteurActions()) {
            JeuController.professeurCourant.setCompteurActions(0);

            if (JeuController.index != Jeu.professeurs.size() - 1) {
                JeuController.professeurCourant = Jeu.professeurs.get(++JeuController.index);
            } else {
                tourEtudiants();
                contenu.getPlateau().repaint(); //Actualise le plateau de jeu (Vue)
            }

            professeurPanel.getChoixActionPanel().resetChoixActionPanel(); //On réinitialise ChoixActionPanel
            professeurPanel.getChoixDeplacementPanel().resetChoixDeplacementPanel(); //On réinitialise ChoixDeplacementPanel
            updateTextEtudiantLabel(); //On met à jour le label de l'étudiant associé au nouveau prof courant
            installerAPoste(); //On vérifie si on a besoin d'activer l'option "Valider UV" pour le nouveau prof courant

        }
    }

    /**
     * Passe à la card du tour des étudiants et exécute leurs actions
     */
    private void tourEtudiants() {
        contenu.getCardLayout().show(contenu.getCardPanel(), "étudiants");
        etudiantsPanel.update(JeuController.jeu.tourEtudiants());
        JeuController.index = -1;
        etatDuJeu();
    }

    /**
     * Méthode permettant d'afficher les Panels de fin de jeu si la partie est gagnée ou perdue
     */
    private void etatDuJeu() {
        String etat = JeuController.jeu.verificationFinTour(); //On récupère l'état du Modèle
        switch (etat) {
            case "Victoire" :
                JeuController.fenetrePrincipale.finDeJeu(true); //On affiche le Panel de victoire
                break;
            case "Défaite" :
                JeuController.fenetrePrincipale.finDeJeu(false); //On affiche le Panel de défaite
                break;
            case "" :
                System.out.println("La partie continue !"); //On continue la partie
                break;
            default : //Si ce cas est atteint, c'est qu'un ou plusieurs professeurs sont KO
                String[] nomProfs = etat.split("-");
                for (String nom:
                     nomProfs) {
                    professeurPanel.affichageProfesseurKO(nom); //On affiche tous les professeurs KO

                    //On enlève maintenant les pions du jeu
                    int indicePion = 0;
                    Pion pion;
                    //On cherche le pion possédant le même nom
                    do {
                        pion = contenu.getPlateau().getPions().get(indicePion);
                        ++indicePion;
                    } while (!pion.getNomProfesseur().equals(nom));

                    if (pion.getNomProfesseur().equals(nom)) {
                        pion.removeZone(); //Le pion s'enlève de sa zone
                        contenu.getPlateau().getPions().remove(pion); //On enlève le pion du plateau
                    }

                }
                break;
        }
    }

    /**
     * Méthode permettant d'effectuer le déplacement voulu par l'utilisateur, à condition que le déplacement soit possible
     * @return un booléen : vrai si le déplacement a été effectué, faux sinon
     */
    private boolean effectuerDeplacement() {
        if (verificationDeplacement()) {
            JeuController.professeurCourant.changerZone(JeuController.zoneCliquee);
            //On déplace le pion
            contenu.getPlateau().getPions().get(JeuController.index).changerZone(JeuController.zoneCliquee - 1);
            return true;
        }

        return false;
    }

    /**
     * Méthode permettant d'effectuer l'installation à un poste
     * @return un booléen : vrai si l'installation a été effectuée, faux sinon
     */
    private boolean effectuerInstallation() {
        if (verificationPoste()) {
            JeuController.professeurCourant.installerEtudiant(JeuController.posteClique);

            //Déplacement des pions
            SallePanel salleActuelle = (SallePanel)contenu.getPlateau().getZones().get(JeuController.zoneCliquee - 1);
            Pion pionCourant = contenu.getPlateau().getPions().get(JeuController.index);
            //On vérifie si le poste cliqué est libre, si non, on switche les deux pions
            if (!salleActuelle.getListPostes().get(JeuController.posteClique - 1).getPions().isEmpty()) {
                pionCourant.switchPoste(salleActuelle.getListPostes().get(JeuController.posteClique - 1).getPions().get(0));
            }
            else {
                pionCourant.changerPoste(JeuController.zoneCliquee,JeuController.posteClique);
            }
            return true;
        }
        return false;
    }

    /**
     * Méthode permettant de savoir si le poste sélectionné est disponible, et se trouve dans la même salle que le professeur
     * @return un booléen : vrai si le poste est disponible, faux sinon
     */
    private boolean verificationPoste() {
        Professeur prof = JeuController.professeurCourant;
        int indiceZoneProf = prof.getZonePosition().getIndiceZone();
        boolean posteDispo = ((Salle)(prof.getZonePosition())).posteEstDisponible(JeuController.posteClique);

        //Si la zone cliquée est la même que la zone de prof ET que le poste sélectionné est disponible
        if (indiceZoneProf == JeuController.zoneCliquee && posteDispo) {
            return true;
        }
        return false;
    }

    /**
     * Méthode permettant de savoir si le déplacement demandé par le joueur est possible ou non
     * @return un booléen : vrai si l'action est possible, faux sinon
     */
    private boolean verificationDeplacement() {

        if (JeuController.zoneCliquee > 0) {

            int position = JeuController.professeurCourant.getZonePosition().getIndiceZone();
            int col = (position % (Jeu.nbrColonnes + 1)); //On obtient l'indice de la colonne

            if (col > 1 && JeuController.zoneCliquee == position - 1) {
                return true;
            }

            if (col < Jeu.nbrColonnes && JeuController.zoneCliquee == position + 1) {
                return true;
            }

            if (JeuController.zoneCliquee == position + Jeu.nbrColonnes || JeuController.zoneCliquee == position - Jeu.nbrColonnes) {
                return true;
            }
        }
        return false;
    }
}
