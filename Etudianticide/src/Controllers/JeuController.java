package Controllers;

import Models.Jeu;
import Models.Plateau;
import Models.Professeur;
import Views.ContenuGlobal;
import Views.FenetrePrincipale;

import javax.swing.*;
import java.util.ArrayList;

public class JeuController {

    public static Jeu jeu;

    public static int index = 0;
    public static Professeur professeurCourant;

    public static int zoneCliquee = -1;
    public static int posteClique = -1;

    public static Plateau plateau;

    public static FenetrePrincipale fenetrePrincipale;

    public JeuController(Jeu jeuModel) {

        jeu = jeuModel;
        plateau = jeu.getPlateau();
        professeurCourant = Jeu.professeurs.get(index);

        fenetrePrincipale = new FenetrePrincipale();
        fenetrePrincipale.update();

        JOptionPane.showMessageDialog(null, "Objectif de cette partie :\n- Survivre pendant " + Jeu.objectifTour + " tours\n- Ne pas avoir plus de 15 étudiants en attente\n\nRécompense :\nUne sucette !!\n\nVous avez intérêt à gagner !", "Objectif", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, "PS: C'était une blague... il n'y a pas de sucette.. \nMais après tout, Confucius a dit : Faire le bien sans chercher de récompense\nC'est donc amplement suffisant de se dire que vous aurez gagné, non ?", "Petite blague", JOptionPane.INFORMATION_MESSAGE);
    }
}
