package Views;

import Controllers.JeuController;
import Models.Jeu;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ProfesseurPanel extends JPanel{

    private JLabel headerLab;
    private JLabel pvLabel;
    private JLabel etudiantLabel; //Affichage de l'étudiant associé au prof courant
    private ExpPanel expPanel;
    private ChoixActionPanel choixActionPanel;
    private ChoixDeplacementPanel choixDeplacementPanel;

    public ProfesseurPanel() {
        initContenu();
        addComposants();
        miseEnForme(LEFT_ALIGNMENT);
    }

    /**
     * Initialise les composants/le contenu du panel
     */
    private void initContenu() {
        headerLab = new JLabel("Tour #" + Jeu.tour + " du professeur : " + JeuController.professeurCourant.getNom(), JLabel.CENTER);
        headerLab.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 15));
        headerLab.setForeground(JeuController.professeurCourant.getCouleurPion());
        pvLabel = new JLabel("Point(s) de vie restant(s) : " + JeuController.professeurCourant.getPv(), JLabel.CENTER);
        pvLabel.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 12));
        etudiantLabel = new JLabel("Aucun étudiant assigné", JLabel.CENTER);
        etudiantLabel.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 12));
        choixActionPanel = new ChoixActionPanel();
        choixDeplacementPanel = new ChoixDeplacementPanel();
        expPanel = new ExpPanel();
    }

    /**
     * Ajoute les composants au panel
     */
    private void addComposants() {
        this.add(headerLab);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(pvLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(etudiantLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(expPanel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(choixActionPanel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
    }

    /**
     * S'occupe de la mise en page
     * @param align précise l'alignement des composants
     */
    private void miseEnForme(float align) {
        headerLab.setAlignmentX(align);
        pvLabel.setAlignmentX(align);
        etudiantLabel.setAlignmentX(align);
        expPanel.setAlignmentX(align);
        choixActionPanel.setAlignmentX(align);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    /**
     * Permet de basculer de choixActionPanel à choixDeplacementPanel, et vice-versa, en jouant avec la validité des Panel
     */
    public void switchActionsDeplacement() {
        if (choixActionPanel.isValid()) {
            this.remove(choixActionPanel);
            this.add(choixDeplacementPanel);
        }
        else {
            this.remove(choixDeplacementPanel);
            this.add(choixActionPanel);
        }
        this.revalidate();
    }

    /**
     * Vérifie si on est dans la choix de l'action du professeur
     * @return true si le panel de choix est présent/false sinon
     */
    public boolean isValidChoixActionPanel() {
        return (choixActionPanel.isValid());
    }

    /**
     * Demande à l'utilisateur de choisir l'étudiant à rencontrer
     * @param etudiants représente les etudiants que le professeur peut rencontrer
     */
    public Object choisirEtudiant(String[] etudiants) {

        final JComboBox<String> combo = new JComboBox<>(etudiants);
        String[] options = { "OK" };
        String title = "Choix de l'étudiant";

        JOptionPane.showOptionDialog(null, combo, title,
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options,
                options[0]);

        return combo.getSelectedItem();
    }
    /**
     * Met à jour le panel
     */
    public void update() {
        headerLab.setText("Tour #" + Jeu.tour + " du professeur : " + JeuController.professeurCourant.getNom());
        headerLab.setForeground(JeuController.professeurCourant.getCouleurPion());
        pvLabel.setText("Point(s) de vie restant(s) : " + JeuController.professeurCourant.getPv());
        expPanel.update();
        if (!etudiantLabel.getText().equals("Aucun étudiant assigné")) {
            ajoutNomEtudiantLabel();
        }
        //Les mises à jour de choixActionPanel et choixDeplacmentPanel se font dans ActionController, en fonction du contexte actuel
//        choixActionPanel.update();
//        choixDeplacementPanel.update();

        this.revalidate();
        this.repaint();
    }

    public ChoixActionPanel getChoixActionPanel() {
        return choixActionPanel;
    }

    public ChoixDeplacementPanel getChoixDeplacementPanel() {
        return choixDeplacementPanel;
    }

    /**
     * Méthode permettant de réinitialiser ChoixActionPanel et ChoixDeplacementPanel
     */
    public void resetProfesseurPanel() {
        choixActionPanel.resetChoixActionPanel();
        choixDeplacementPanel.resetChoixDeplacementPanel();
    }

    /**
     * Méthode permettant dde redéfinir le texte de etudiantLabel avec le nom de l'étudiant attribué
     */
    public void ajoutNomEtudiantLabel() {
        etudiantLabel.setText("Etudiant associé : " + JeuController.professeurCourant.getEtudiant().getNom() + " | PV : " + JeuController.professeurCourant.getEtudiant().getPv());
    }

    /**
     * Méthode permettant de redéfinir le texte de etudiantPanel avec une valeur par défaut
     */
    public void reinitialiserNomEtudiantLabel() {
        etudiantLabel.setText("Aucun étudiant assigné");
    }

    /**
     * Méthode permettant d'afficher une pop-up contenant le nom du professeur KO
     * @param professeur correspond au nom du professeur KO
     */
    public void affichageProfesseurKO(String professeur) {
        JOptionPane.showMessageDialog(null, "Le stress a conduit " + professeur + " au burn-out... Il a quitté l'UTBM, étant l'objet d'une dépression abyssale !",
                "Professeur en burn-out", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Méthode permettant d'afficher une pop-up informant l'utilisateur du résultat de son action "valider les UVs"
     * @param text correspond au message à afficher
     */
    public void affichageLancerDe(String text) {
        JOptionPane.showMessageDialog(null, text,
                "Validation des UVs", JOptionPane.INFORMATION_MESSAGE);
    }

}
