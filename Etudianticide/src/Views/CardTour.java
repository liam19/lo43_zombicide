package Views;

import javax.swing.*;
import java.awt.*;

public class CardTour extends JPanel{
    private ProfesseurPanel professeurPanel = new ProfesseurPanel();
    private EtudiantsPanel etudiantsPanel = new EtudiantsPanel();
    private JButton ok = new JButton("Valider");

    private final CardLayout cardLayout = new CardLayout();
    private final JPanel cardPanel = new JPanel(cardLayout);

    public CardTour() {
        cardPanel.add(professeurPanel, "professeur");
        cardPanel.add(etudiantsPanel,"étudiants");
        this.add(cardPanel, BorderLayout.EAST);
        this.add(ok);
        miseEnForme();
    }

    private void miseEnForme() {
        cardPanel.setAlignmentX(LEFT_ALIGNMENT);
        ok.setAlignmentX(LEFT_ALIGNMENT);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 15));
        this.setPreferredSize(new Dimension(290,150));
    }

    public ProfesseurPanel getProfesseurPanel() {
        return professeurPanel;
    }

    public EtudiantsPanel getEtudiantsPanel() {
        return etudiantsPanel;
    }

    public JButton getOk() {
        return ok;
    }

    public CardLayout getCardLayout() {
        return cardLayout;
    }

    public JPanel getCardPanel() {
        return cardPanel;
    }

}
