package Views;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ChoixActionPanel extends JPanel {

    private ArrayList<JRadioButton> listOptions;

    public ChoixActionPanel() {
        addLabelAction();
        this.add(Box.createRigidArea(new Dimension(0, 5)));
        initActions();
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    /**
     * Crée le groupe de boutons radios correspondant aux action disponibles
     * et l'affiche dans le panel
     */
    private void initActions() {
        JRadioButton option1 = new JRadioButton("Ne rien faire", true);
        JRadioButton option2 = new JRadioButton("Valider UV");
        JRadioButton option3 = new JRadioButton("Trouver équipement");
        JRadioButton option4 = new JRadioButton("Se déplacer");

        option2.setEnabled(false);

        ButtonGroup group = new ButtonGroup();
        group.add(option1);
        group.add(option2);
        group.add(option3);
        group.add(option4);

        listOptions = new ArrayList<>();
        listOptions.add(option1);
        listOptions.add(option2);
        listOptions.add(option3);
        listOptions.add(option4);

        add(option1);
        add(option2);
        add(option3);
        add(option4);
    }

    /**
     * Ajoute un titre avant les boutons radios
     */
    private void addLabelAction() {
        JLabel jlab = new JLabel("Choisir une action :", JLabel.CENTER);
        jlab.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 13));
        this.add(jlab);
    }

    public ArrayList<JRadioButton> getListOptions() {
        return listOptions;
    }

    /**
     * Selectionne par défaut l'option 1 après une validation
     */
    public void update() {
        listOptions.get(0).setSelected(true);
    }

    /**
     * Méthode permettant de réinitialiser ChoixActionPanel avec des valeurs de JRadioButtons par défaut
     */
    public void resetChoixActionPanel() {
        listOptions.get(0).setEnabled(true);
        listOptions.get(0).setSelected(true);
        listOptions.get(1).setEnabled(false);
        listOptions.get(2).setEnabled(true);
        listOptions.get(3).setEnabled(true);
    }

    /**
     * Méthode permettant d'activer uniquement les JRadioButtons Valider les UVs et Trouver un équipement
     */
    public void enableValiderUV() {
        listOptions.get(0).setEnabled(false);

        //On active et sélectionne uniquement les JRadioButtons des choix ValiderUV et trouverEquipement
        listOptions.get(1).setEnabled(true);
        listOptions.get(1).setSelected(true);
        listOptions.get(2).setEnabled(true);

        listOptions.get(3).setEnabled(false);
    }
}
