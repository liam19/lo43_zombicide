package Views;

import Models.Jeu;
import Models.Professeur;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class PlateauView extends JPanel {

    private ArrayList<CouloirPanel> listCouloirs;
    private ArrayList<SallePanel> listSalles;
    private ArrayList<Pion> pions;
    private ArrayList<ZonePanel> zones;

    public PlateauView() {

        listCouloirs = new ArrayList<>();
        listSalles = new ArrayList<>();
        zones = new ArrayList<>();
        pions = new ArrayList<>();
        this.gridInit();
    }

    /**
     * Crée une grille de boutons (=zones) sur le JPanel
     */
    private void gridInit() {

        this.setLayout(new GridLayout(Jeu.nbrLignes, Jeu.nbrColonnes, 2, 2));

        int i = 1;
        while (i <= Jeu.nbrLignes * Jeu.nbrColonnes/2 ) {
            createCouloir(i);
            ++i;
        }
        while (i <= Jeu.nbrLignes * Jeu.nbrColonnes) {
            createSalle(i,Jeu.nbrPostes,3);
            ++i;
        }

        createAndAddPions();
    }

    /**
     * Crée un bouton couloir avec l'index donné
     * @param index numéro du couloir (=indicePosition)
     */
    private void createCouloir(int index) {
        CouloirPanel couloir = new CouloirPanel("Couloir", index);
        this.listCouloirs.add(couloir);
        this.zones.add(couloir);
        this.add(couloir);
    }

    /**
     * Crée une salle avec un index et avec le nombre de postes indiqué
     * @param index index de la salle
     * @param nbPostes nombre de postes dans la salle
     * @param nbLignes nombre de lignes sur lesquelles sont répartis les postes
     */
    private void createSalle(int index, int nbPostes, int nbLignes) {
        SallePanel salle = new SallePanel("Salle", index, nbPostes, nbLignes);
        this.listSalles.add(salle);
        this.zones.add(salle);
        this.add(salle);
    }

    private void createAndAddPions() {
        for (Professeur professeur:
             Jeu.professeurs) {
            this.pions.add(new Pion(professeur.getNom(),this));
        }
    }

    public ArrayList<CouloirPanel> getListCouloirs() {
        return listCouloirs;
    }

    public ArrayList<SallePanel> getListSalles() {
        return listSalles;
    }

    public ArrayList<ZonePanel> getZones() {
        return zones;
    }

    public ArrayList<Pion> getPions() {
        return pions;
    }
}
