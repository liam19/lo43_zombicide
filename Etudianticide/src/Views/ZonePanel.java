package Views;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ZonePanel extends JPanel {
    protected String nom;
    protected int index;
    protected ArrayList<Pion> pions;

    public ZonePanel(String nom, int index) {
        this.nom = nom;
        this.index = index;
        this.pions = new ArrayList<>();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }

    public String getNom() {
        return nom;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return nom + " " + index;
    }

    public ArrayList<Pion> getPions() {
        return pions;
    }

    public void addPion(Pion pion) {
        this.pions.add(pion);
        repaint();
    }

    public void removePion(Pion pion) {
        this.pions.remove(pion);
        repaint();
    }


}
