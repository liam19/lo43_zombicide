package Views;

import Controllers.JeuController;

import javax.swing.*;
import java.awt.*;

public class ExpPanel extends JPanel {

    private JLabel labelLvl;
    private JLabel labelExp;
    private JProgressBar barExp;


    public ExpPanel() {
        initComposants();
        addComposants();
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    }

    /**
     * Initialise les composants aux valeurs par défaut
     */
    private void initComposants() {
        labelLvl = new JLabel("LVL : " + JeuController.professeurCourant.getNiveau(), JLabel.CENTER);
        labelLvl.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 12));

        labelExp = new JLabel("Exp : ", JLabel.CENTER);
        labelExp.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 12));

        barExp = new JProgressBar(0,5);
        barExp.setValue(0);
        barExp.setStringPainted(true);
        barExp.setString("0/5");
        barExp.setMaximumSize(new Dimension(300,20));
    }

    /**
     * Ajoute les composants au panel
     */
    private void addComposants() {
        this.add(labelLvl);
        this.add(Box.createRigidArea(new Dimension(10, 0)));
        this.add(labelExp);
        this.add(barExp);
    }

    /**
     * Met à jour les composants en fonction du professeur courant
     */
    private void updateComposants() {
        labelLvl.setText("LVL : " + JeuController.professeurCourant.getNiveau());

        int exp = JeuController.professeurCourant.getXp();
        barExp.setValue(exp);
        barExp.setString(exp + "/5");
        this.add(barExp);
    }

    /**
     * Met à jour le panel d'expérience du professeur courant
     */
    public void update() {
        updateComposants();
    }
}
