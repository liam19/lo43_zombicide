package Views;

import Controllers.EquipementController;

import javax.swing.*;

public class EquipementView {

    private EquipementController equipementController;

    public EquipementView() {

        this.equipementController = new EquipementController(this);
    }

    /**
     * Demande à l'utilisateur s'il veut utiliser un équipement
     */
    public void utiliserEquipement() {

        equipementController.utilisationEquipement(JOptionPane.showConfirmDialog(null, "Voulez-vous utiliser un objet d'équipement ?", "Utilisation objet d'équipement",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE));
    }

    /**
     * Demande à l'utilisateur de choisir l'équipement à utiliser
     * @param equipements représente l'inventaire du joueur courant parmi lequel il va choisir un équipement
     */
    public void choisirEquipement(String[] equipements) {

        final JComboBox<String> combo = new JComboBox<>(equipements);
        String[] options = { "OK" };
        String title = "Choix de l'équipement";

        JOptionPane.showOptionDialog(null, combo, title,
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options,
                options[0]);

        equipementController.choixEquipement(combo.getSelectedItem());
    }

    /**
     * Affiche une popUp avec l'explication sur l'équipement
     * @param equip donne les caractéristtiques de l'équipement trouvé
     */
    public void trouverEquipement(String equip) {
        JOptionPane.showMessageDialog(null, equip, "Equipement trouvé", JOptionPane.INFORMATION_MESSAGE);
    }

    public void pasEquipement() {
        JOptionPane.showMessageDialog(null,
                "Votre inventaire étant vide, vous allez essayer de valider les UV sans équipement.",
                "Pas d'équipement disponible",
                JOptionPane.WARNING_MESSAGE);
    }
}
