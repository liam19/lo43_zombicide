package Views;

import Controllers.ActionController;
import Models.Jeu;
import Models.Plateau;

import javax.swing.*;
import java.awt.*;

public class ContenuGlobal extends JPanel {

    private CardTour cardTour = new CardTour();
    private PlateauView plateau;

    public ContenuGlobal() {
        setLayout(new BorderLayout());
        initAndAddComposants();
        setBorder(BorderFactory.createEmptyBorder(10, 30, 20, 30)); //ajoute un gap entre le panel et le frame
    }

    /**
     * Ajoute tous les composants au contenu
     */
    private void initAndAddComposants() {
        initAndAddHeader();
        plateau = new PlateauView();
        this.add(plateau, BorderLayout.CENTER);
        initAndAddCardTour();
    }

    /**
     * Crée et ajoute au panel un header permettant d'afficher le titre en haut de la fenêtre
     */
    private void initAndAddHeader() {

        JLabel headerLabel = new JLabel("Bienvenue dans Etudianticide !", JLabel.CENTER);
        headerLabel.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 20));
        headerLabel.setBorder(BorderFactory.createEmptyBorder(5, 0, 10, 0));

        JPanel headerPanel = new JPanel();
        headerPanel.add(headerLabel);
        this.add(headerPanel, BorderLayout.NORTH);
    }

    /**
     * Crée et ajoute un cardlayout avec le panel des professeurs et celui des étudiants
     */
    private void initAndAddCardTour() {
        cardTour.getOk().addActionListener(new ActionController(this));
        this.add(cardTour, BorderLayout.EAST);
        revalidate();
    }

    public ProfesseurPanel getProfesseurPanel() {
        return cardTour.getProfesseurPanel();
    }

    public EtudiantsPanel getEtudiantPanel() {
        return cardTour.getEtudiantsPanel();
    }

    public CardLayout getCardLayout() {
        return cardTour.getCardLayout();
    }

    public JPanel getCardPanel() {
        return cardTour.getCardPanel();
    }

    public PlateauView getPlateau() {
        return plateau;
    }
}