package Views;

import Controllers.JeuController;
import Models.Salle;
import Models.Zone;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Pion {
    private String nomProfesseur;
    private Image imagePion;
    private PlateauView plateau;
    private ZonePanel zone;
//    private int positionX;
//    private int positionY;

    public Pion(String nomProfesseur, PlateauView plateau) {
        this.nomProfesseur = nomProfesseur;
        String path = "Resources/Pictures/" + nomProfesseur + ".png";
        try {
            imagePion = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //On s'ajoute à PlateauView
        this.plateau = plateau;

        //On récupère et on s'ajoute à la première zone
        this.zone = plateau.getZones().get(0);
        this.zone.addPion(this);
    }

    public Image getImagePion() {
        return imagePion;
    }

    /**
     * Méthode permettant de changer le imagePion d'un zone à une autre
     * @param indiceZone correspond à l'indice de la zone
     */
    public void changerZone(int indiceZone) {
        this.zone.removePion(this);
        this.zone = plateau.getZones().get(indiceZone);
        if (this.zone instanceof SallePanel) {
            int i = 0;
            //On vérifie que le poste et disponible et qu'aucun pion ne se trouve déjà sur le poste
            while (!((SallePanel) this.zone).getListPostes().get(i).getPions().isEmpty()) {
                ++i;
            }
            this.zone = ((SallePanel) this.zone).getListPostes().get(i);
        }
        this.zone.addPion(this);
    }

    /**
     * Méthode permettant de changer de poste
     * @param indiceSalle correspond à l'indice de la salle
     * @param indicePoste correspond à l'indice du poste sur lequel on souhaite déplacer notre pion
     */
    public void changerPoste(int indiceSalle, int indicePoste) {
        this.zone.removePion(this);
        this.zone = ((SallePanel)plateau.getZones().get(indiceSalle - 1)).getListPostes().get(indicePoste - 1);
        this.zone.addPion(this);
    }

    /**
     * Méthode permettant d'intervertir les zones du pion actuel et d'un autre pion
     * @param pion correspond à l'autre pion
     */
    public void switchPoste(Pion pion) {
        PostePanel posteCourant = (PostePanel)this.zone;
        PostePanel posteAutrePion = (PostePanel)pion.zone;

        this.zone = posteAutrePion;
        this.zone.removePion(pion);
        this.zone.addPion(this);

        pion.zone = posteCourant;
        pion.zone.removePion(this);
        pion.zone.addPion(pion);
    }

    public String getNomProfesseur() {
        return nomProfesseur;
    }

    public void removeZone() {
        this.zone.removePion(this);
        this.zone = null;
    }
}
