package Views;

import javax.swing.*;

public class EndGamePanel {

    public EndGamePanel(boolean victoire) {
        if (victoire) {
            JOptionPane.showMessageDialog(null, "Vous êtes une vraie pépite !! Votre aptitude à gérer les situations délicates vous a permis de gagner cette partie... Vous avez gagné !!", "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Votre aptitude à gérer cette partie fut désastreuse... Vous avez perdu...", "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
        }
    }

}
