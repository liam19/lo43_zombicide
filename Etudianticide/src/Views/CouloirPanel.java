package Views;

import Controllers.CouloirListener;
import Controllers.JeuController;
import Models.Jeu;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class CouloirPanel extends ZonePanel {

    public CouloirPanel(String nom, int index) {
        super(nom,index);
        createAndAddNomZone();
    }

    private void createAndAddNomZone() {
        JLabel nomZone = new JLabel(this.toString());
        nomZone.setFont(new Font(this.getFont().getFontName(), Font.ITALIC, 9));
        this.addMouseListener(new CouloirListener(this));
        this.add(nomZone);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //Définition du fond des couloirs
        Graphics2D g2d = (Graphics2D)g;
        Image image = null;
        try {
            if (JeuController.plateau.getZone(this.index).contientEtudiants()) {
                image = ImageIO.read(new File("Resources/Pictures/couloir_danger.png"));
            }
            else {
                image = ImageIO.read(new File("Resources/Pictures/couloir_carré.png"));
            }
        }
        catch (IOException e) {

        }
        g2d.drawImage(image, 0, 0, getWidth(), getHeight(), null);

        //Affichage de tous les pions présents dans la zone
        int x = 0, y = 0;

        for (Pion pion :
                pions) {
            g2d.drawImage(pion.getImagePion(), x, y, getWidth() / Jeu.nbrColonnes, getHeight() / Jeu.nbrLignes, null);
            x += getWidth() / Jeu.nbrColonnes + 3;
            if (x > getWidth()) {
                y += getHeight() / Jeu.nbrLignes;
                x = 0;
            }
        }
    }

}
