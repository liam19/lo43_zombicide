package Views;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ChoixDeplacementPanel extends JPanel {
    private ArrayList<JRadioButton> listChoix;
    private JRadioButton option1 = new JRadioButton("Changer de zone", true);
    private JRadioButton option2 = new JRadioButton("Rencontrer un étudiant");
    private JRadioButton option3 = new JRadioButton("Installer un étudiant");
    private JLabel message = new JLabel("Sélectionner une zone et la valider");


    public ChoixDeplacementPanel() {
        message.setForeground(Color.RED);

        ajoutLabelTitre();
        this.add(Box.createRigidArea(new Dimension(0, 5)));
        initialiserChoixDeplacementPanel();
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    private void ajoutLabelTitre() {
        JLabel titre = new JLabel("Choisir une action de déplacement :", JLabel.CENTER);
        titre.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 13));
        this.add(titre, BorderLayout.NORTH);
    }

    private void initialiserChoixDeplacementPanel() {
        option2.setEnabled(false);
        option3.setEnabled(false);

        ButtonGroup group = new ButtonGroup();
        group.add(option1);
        group.add(option2);
        group.add(option3);

        listChoix = new ArrayList<>();
        listChoix.add(option1);
        listChoix.add(option2);
        listChoix.add(option3);

        add(option1);
        add(option2);
        add(option3);
    }

    public void update() {
        listChoix.get(0).setSelected(true);
    }

    public ArrayList<JRadioButton> getListChoix() {
        return listChoix;
    }


    public void addMessage(String textMessage) {
        message.setText(textMessage);
        this.add(message);
        this.revalidate();
    }

    public boolean containsMessageDeplacemennt() {
        return message.isValid();
    }

    public void removeMessageDeplacement() {
        this.remove(message);
        this.revalidate();
    }

    public void errorDeplacement() {
        JOptionPane.showMessageDialog(null, "Zone inaccessible à partir de la position actuelle",
                "Zone inaccessible", JOptionPane.ERROR_MESSAGE);
    }

    public void resetChoixDeplacementPanel() {
        option1.setEnabled(true);
        option1.setSelected(true);
        option2.setEnabled(false);
        option3.setEnabled(false);
    }

    public void enableRencontrerEtudiant() {
        listChoix.get(1).setEnabled(true);
    }

    public void disableRecontrerEtudiant() {
        listChoix.get(1).setEnabled(false);
        listChoix.get(0).setSelected(true);
    }

    public void enableInstallerEtudiant() {
        listChoix.get(2).setEnabled(true);
        listChoix.get(2).setSelected(true);
    }

    public void disableInstallerEtudiant() {
        listChoix.get(2).setEnabled(false);
        listChoix.get(0).setSelected(true);
    }

    public void enableChangerZone() {
        listChoix.get(0).setEnabled(true);
        listChoix.get(0).setSelected(true);
    }

    public void disableChangerZone() {
        listChoix.get(0).setEnabled(false);
        listChoix.get(2).setSelected(true);

    }

}