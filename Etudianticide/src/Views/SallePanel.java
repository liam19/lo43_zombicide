package Views;

import Controllers.JeuController;
import Controllers.PosteListener;
import Controllers.SalleListener;
import Models.Jeu;
import Models.Poste;
import Models.Salle;
import Models.Zone;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SallePanel extends ZonePanel {

    private ArrayList<PostePanel> listPostes = new ArrayList<>();

    public SallePanel(String nom, int index, int nbPostes, int nbLignes) {
        super(nom, index);
        addEntete();
        grilleInit(nbPostes, nbLignes);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        this.addMouseListener(new SalleListener(this));
    }

    /**
     * Initialise la salle avec un gridLayout comportant un certain
     * nombre de postes répartis sur un certain nombre de lignes
     * @param nbPostes nombre de postes désirés dans la salle
     * @param nbLignes nombre de lignes sur lesquelles sont répartis les postes
     */
    private void grilleInit(int nbPostes, int nbLignes) {
        JPanel grille = new JPanel();

        grille.setLayout(new GridLayout(nbLignes, nbPostes/nbLignes));

        for (int i = 1; i <= nbPostes; i++) {

            PostePanel posteButton = new PostePanel(i, index, this);
            this.listPostes.add(posteButton);
            grille.add(posteButton);
        }
        this.add(grille);

    }

    private void addEntete() {
        JLabel entete = new JLabel("Salle " + index);
        entete.setPreferredSize(new Dimension(10,25));
        entete.setAlignmentX(CENTER_ALIGNMENT);
        this.add(entete);
    }

    public ArrayList<PostePanel> getListPostes() {
        return listPostes;
    }

    public int getIndexSalle() {
        return index;
    }

    @Override
    public void addPion(Pion pion) {
        this.pions.add(pion);
        Salle zoneModel = (Salle)JeuController.plateau.getZone(this.index);
        int i = 0;

        while (!zoneModel.posteEstDisponible(i)) {
            ++i;
        }

        listPostes.get(i).addPion(pion);
    }

    @Override
    public void removePion(Pion pion) {

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //Définition du fond des salles
        Graphics2D g2d = (Graphics2D)g;
        Image image = null;
        try {
            image = ImageIO.read(new File("Resources/Pictures/salle.png"));
        }
        catch (IOException e) {

        }
        g2d.drawImage(image, 0, 0, getWidth(), getHeight(), null);
    }
}
