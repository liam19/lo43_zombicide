package Views;

import javax.swing.*;
import java.awt.*;

public class EtudiantsPanel extends JPanel {

    private JLabel headerLab = new JLabel("Tour des étudiants: ", JLabel.CENTER);
    private  JTextArea textAction = new JTextArea (5, 30);
    private JScrollPane actionScroll = new JScrollPane (textAction);

    public EtudiantsPanel(){
        addComposants();
        miseEnForme(LEFT_ALIGNMENT);
    }

    /**
     * Ajoute les composants au panel
     */
    private void addComposants() {
        this.add(headerLab);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add (actionScroll, BorderLayout.SOUTH);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
    }

    private void miseEnForme(float align) {
        this.setPreferredSize(new Dimension(290,150));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        headerLab.setFont(new Font(this.getFont().getFontName(), Font.BOLD, 15));
        headerLab.setAlignmentX(align);

        textAction.setEnabled(false);
        textAction.setDisabledTextColor(Color.black);
        textAction.setLineWrap(true);
        textAction.setBorder(null);
        textAction.setOpaque(false);
        textAction.setWrapStyleWord(true);
        textAction.setBorder(BorderFactory.createEmptyBorder(2, 5, 1, 5));
        actionScroll.setAlignmentX(align);
        actionScroll.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }

    public void update(String logs) {
        textAction.setText(logs);
        textAction.setCaretPosition(0);
    }
}
