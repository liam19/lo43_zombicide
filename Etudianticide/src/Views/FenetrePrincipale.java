package Views;

import Controllers.MenuController;

import javax.swing.*;
import java.awt.*;

public class FenetrePrincipale extends JFrame {

    private MenuController menuController;
    private ContenuGlobal contenuGlobal;

    private JMenuBar menuBar;

    private JMenu fichierMenu;
    private JMenu aideMenu;

    public JMenuItem fermerItem;
    public JMenuItem reglesItem;

    public JScrollPane reglesScrollPane;

    public FenetrePrincipale() {
        initAttributes();
        draw();
    }

    /**
     * Initialise tous les parametres, attributs et composants de la vue principale
     */
    private void initAttributes() {

        this.menuController = new MenuController(this);

        //initialise les parametres de la fenetre
        this.setTitle("Etudianticide");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //initialise le contenu
        this.contenuGlobal = new ContenuGlobal();

        //initialise les composants
        this.menuBar = new JMenuBar();

        this.fichierMenu = new JMenu("Fichier");
        this.aideMenu = new JMenu("Aide");

        this.fermerItem = new JMenuItem("Fermer");
        this.reglesItem = new JMenuItem("Règles du jeu");

        reglesScrollPane = new JScrollPane(new Regles());
        reglesScrollPane.setBorder(null);

        // ------ Menu Bar ------
        this.fermerItem.addActionListener(menuController);
        this.fichierMenu.add(fermerItem);
        this.menuBar.add(fichierMenu);

        this.reglesItem.addActionListener(menuController);
        this.aideMenu.add(reglesItem);
        this.menuBar.add(aideMenu);

        this.setJMenuBar(menuBar);
    }

    /**
     * Affiche les objets précédemment définis sur l'écran
     */
    private void draw() {

        this.setContentPane(this.contenuGlobal);
        this.pack();
        this.setCenter();
        this.setVisible(true);
    }

    /**
     * Affiche la fenêtre au centre de l'écran
     */
    private void setCenter() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }

    /**
     * Met à jour le contenu de la fenêtre
     */
    public void update() {
        this.pack();
        this.setCenter();
    }

    public void finDeJeu(boolean victoire) {
        new EndGamePanel(victoire);
        dispose();
    }
}
