package Views;

import javax.swing.*;
import java.awt.*;

public class Regles extends JPanel {

    private JScrollPane scrollPane;

    public Regles() {
        miseEnPage();
        this.add(scrollPane);
    }

    private void miseEnPage() {
        JTextArea textArea = new JTextArea(getRegles(), 20, 45);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setEnabled(false);
        textArea.setDisabledTextColor(Color.black);
        textArea.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        textArea.setOpaque(false);
        scrollPane = new JScrollPane(textArea);
        scrollPane.setBorder(null);
    }

    /**
     * get the rules
     * @return the string comporting the rules of the game
     */
    private static String getRegles() {
        return "# Victoire\n" +
                "Les  professeurs ont \"survécu\" aux 15 tours.\n" +
                "\n" +
                "# Défaite\n" +
                "Tous les professeurs sont en burn-out (KO).\n" +
                "\n" +
                "# Déroulement de la partie\n" +
                "\n" +
                "La partie se découpe en tour :\n" +
                "   - apparition des étudiants (sauf pour le premier tour consacré aux premières actions des joueurs)\n" +
                "   - actions des joueurs\n" +
                "\n" +
                "Chaque membre du personnel a le droit à 3 actions par tour parmi celles listées ci-dessous :\n" +
                "   - Valider les UVs\n" +
                "   - Chercher un objet\n" +
                "   - Ne rien faire\n" +
                "   - Se déplacer, qui se découpe en 3 sous-actions : changer de salle, rencontrer un étudiant, installer un étudiant\n" +
                "\n" +
                "Pour effectuer l'action \"Valider les UVs\", il est nécessaire d'être installé à un poste suite à l'action \"Installer un étudiant\"," +
                "puis de lancer un dé qui détermine la réussite de l'action. Si le joueur décide d'utiliser un équipement, le résultat à obtenir sera alors plus faible. \n" +
                "\n" +
                "L'attribution de l'expérience dépend du type de l'étudiant.\n" +
                "\n" +
                "Lorsque que tous les joueurs ont fini leur tour, les étudiants rentrent en jeu. \n" +
                "Il y a plusieurs types d'étudiants :\n" +
                "   - Préparé (1 point de vie, 1 point de deplacement, 1 point de puissance)\n" +
                "   - Impatient (1 point de vie, 2 points de déplacement, 1 point de puissance)\n" +
                "   - Porteur de problèmes (5 point de vie, 1 points de déplacement, 3 point de puissance)\n" +
                "\n" +
                "Les actions des étudiants sont : \n" +
                "- Casser les Noix: diminue les points de vie du professeur \"attaqué\"\n" +
                "- se déplacer : le déplacement s'effectue de manière autonome dans les couloirs\n" +
                "\n" +
                "Note : les étudiants privilégieront le déplacement tant qu'ils ne sont pas à leur salle cible sinon ils patientent, mais si , ils sont installé à un poste ils casseront les noix.";
    }
}
