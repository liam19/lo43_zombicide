package Views;

import Controllers.PosteListener;
import Models.Jeu;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class PostePanel extends ZonePanel {

    private int indexSalle;

    public PostePanel(int index, int indexSalle, SallePanel sallePanel) {
        super("Poste", index);
        this.indexSalle = indexSalle;
        setPreferredSize(new Dimension(90,40));
        setBackground(Color.WHITE);
        addMouseListener(new PosteListener(this, sallePanel));
    }

    public int getIndexSalle() {
        return indexSalle;
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //Définition du fond des couloirs
        Graphics2D g2d = (Graphics2D)g;
        Image image = null;
        try {
            image = ImageIO.read(new File("Resources/Pictures/ordinateur_allumé_2.png"));
        }
        catch (IOException e) {}

        g2d.drawImage(image, 0, 0, getWidth(), getHeight(), null);

        //Affichage de tous les pions présents dans la zone
        int x = 0, y = 0;

        for (Pion pion :
                pions) {
            g2d.drawImage(pion.getImagePion(), x, y, getWidth()/2, getHeight(), null);
            x += getWidth() / Jeu.nbrColonnes + 3;
            if (x > getWidth()) {
                y += getHeight() / Jeu.nbrLignes;
                x = 0;
            }
        }

    }
}
