package Models;

public class Prepare extends Etudiant {

    /**
     * Constructeur par défaut
     */
    public Prepare() {
        super();
    }

    public Prepare(int zone, Plateau plateau) {
        super(zone, plateau);
    }

    @Override
    public String toString() {
        return super.toString() + "préparé(e)";
    }
}