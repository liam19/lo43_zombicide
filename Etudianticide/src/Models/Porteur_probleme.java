package Models;

public class Porteur_probleme extends Etudiant {

    /**
     * Constructeur par défaut
     */
    public Porteur_probleme(){
        super();
        initialisePorteurProbleme();
    }

    public Porteur_probleme(int zone, Plateau plateau){
        super(zone,plateau);
        initialisePorteurProbleme();
    }

    /**
     * Initialise les stats de Porteur de Problèmes à ses valeurs qui diffèrent
     */
    public void initialisePorteurProbleme() {
        this.pv = 5;
        this.puissance = 3;
    }

    @Override
    public String toString() {
        return super.toString() + "porteur(euse) de problèmes";
    }
}
