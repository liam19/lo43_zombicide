package Models;

import java.awt.*;
import java.util.ArrayList;

public class Professeur extends Personne {

	private Etudiant etudiant;
	private ArrayList<Equipement> inventaire;
	private boolean enBurnOut = false; //Le statut permet de savoir si le professeur est encore "en vie"
	private int xp = 0;
	private int niveau = 1;
	private Equipement equipementCourant;
	private boolean installerAPoste = false;
	private int indicePosteOccupe;
	private Color couleurPion;


	/**
	 * Contructeur par défaut
	 */
	public Professeur() {
		nbActions = 3;
		pv = 5;
		puissance = 1;
		vitesse = 1;
		etudiant = null;
		inventaire = new ArrayList<>();
		equipementCourant = null;
		nom="Defaut";
		inventaire.add(new Equipement());
		zonePosition = null;
		plateau = null;
		indicePosteOccupe = -1;
		couleurPion = null;
	}

	public Professeur(String nom, Color couleur, int zoneDeDepart, Plateau plateau) {
		this();
		this.nom = nom;
		this.plateau = plateau;
		this.zonePosition = this.plateau.getZone(zoneDeDepart); //On récupère la bonne zone à partir du Plateau
		this.zonePosition.ajouterProfesseur(this); //Et on ajoute le Professeur à la Arraylist contenue dans Zone
		this.couleurPion = couleur;
	}

	/**
	 * On ajoute un nouvel Equipement à l'inventaire
	 */
	public String trouverEquipement() {
		Equipement equip = new Equipement();
		inventaire.add(equip);
		String equipementTrouve = this.nom + " a trouvé " + equip.getNom() + " faisant "
				+ equip.getDegat() + " dégats pour un résultat de jet de dé supérieur à " + equip.getTaux_reussite();
		System.out.println(equipementTrouve);
		return equipementTrouve;
	}


	/**
	 * Attaque du professeur
	 * On peut choisir entre sélectionner un équipement ou non pour effectuer l'action
	 * @param valeurDe correspond à la valeur du Dé lancé
	 * @return un booléen permettant de savoir si l'étudiant est KO ou non
	 */
	public boolean validerUv(int valeurDe) {
		System.out.println("On lance le dé !");
		System.out.println("La valeur du dé est de " + valeurDe);
		if (equipementCourant != null) { //Si un équipement courant est défini
			//Si la valeur du dé est est supérieure au taux de réussite de l'équipement courant, alors on applique les dégâts de l'équipement
			if (valeurDe > equipementCourant.getTaux_reussite()) {
				gagnerXP(this.etudiant.puissance);
				etudiant.diminuerPV(equipementCourant.getDegat());
			}
		}
		else { //Si pas d'équipement, les dégâts par défaut seront appliqués
			if (valeurDe == 6) {
				etudiant.diminuerPV(1);
			}
		}
		//Si l'étudiant est KO, on retourne VRAI
		if (etudiant.getPv() <= 0) {
			indicePosteOccupe = ((Salle)zonePosition).libererPoste(indicePosteOccupe); //On libère le poste
			etudiant = null;
			installerAPoste = false;
			return true;
		}
		return false;
	}

	/**
	 * Permet d'installer le Professeur et l'Etudiant au Poste indiqué
	 * @param poste correspond à l'indice du Poste
	 */
	public void installerEtudiant(int poste) {
		if (zonePosition instanceof Salle) { //Il nous faut savoir si zonePosition est une salle
			indicePosteOccupe = ((Salle) zonePosition).occuperPoste(poste,this,this.etudiant); //On ajoute le Professeur et l'étudiant associé au Poste souhaité
			installerAPoste = true;
		}
	}

	/**
	 *	Permet "d'associer" un Etudiant à un Professeur, afin de Valider ses UVs
	 * @param nomEtudiant correspond au toString d'un des étudiants dans la Zone indiquée par zonePosition
	 */
	public void rencontrerEtudiant(String nomEtudiant) {
		if (nomEtudiant != null) {
			int i = 0;
			ArrayList<Etudiant> etudiants = zonePosition.getEtudiants();

			while (!etudiants.get(i).toString().equals(nomEtudiant) && i < etudiants.size()) {
				i++;
			}
			if (etudiants.get(i).toString().equals(nomEtudiant)) {
				this.etudiant = zonePosition.recupererEtudiant(i);
				this.etudiant.setProfesseur(this); //On attribue notre professeur à l'étudiant récupéré
				System.out.println("L'étudiant " + this.etudiant.nom + " a bien été affecté au professeur " + nom);
			}

		} else {
			this.etudiant.setProfesseur(null);
			this.etudiant = null;
		}
	}

	/**
	 * Permet de récupérer un tableau de String contenant les noms des étudiants dispo dans la zone
	 * @return tableau de String contenant les noms des étudiants dispo dans la zone
	 */
	public String[] recupererNomsEtudiants() {
	    try {
	    	return zonePosition.recupererNomsEtudiants();
		} catch (NullPointerException e) {
	    	return new String[0];
		}
    }

	/**
	 * Méthode permettant de changer la position du Professeur, et celle de l'étudiant associé si il existe
	 * @param zone correspond à la Zone dans laquelle on souhaite se déplacer
	 */
	public void changerZone(int zone) {
	    zonePosition.retirerProfesseur(this); //On enlève le Professeur de la Zone actuelle
		zonePosition = plateau.getZone(zone); //On récupère la Zone voulue dans le Plateau
		zonePosition.ajouterProfesseur(this); //On ajoute le Professeur à la nouvelle Zone
		System.out.println(nom + " a changé de zone ! Il est maintenant dans la zone #" + zonePosition.getIndiceZone());

		if (etudiant != null) { //Si un étudiant est associé, on le fait se déplacer en même temps que le prof
			etudiant.zonePosition.retirerEtudiant(etudiant);
			etudiant.zonePosition = this.zonePosition;
			etudiant.zonePosition.ajouterEtudiant(etudiant);
			System.out.println(etudiant.nom + " s'est déplacé(e) avec " + nom);
		}
	}

	/**
	 * Permet d'améliorer les stats du Professeur (compétence = buff)
	 */
	private void gagnerCompetence() {
		switch ((int) (Math.random() * 4) + 1) {
			case 1 : pv += niveau;
				break;
			case 2 : puissance += niveau;
				break;
			case 3 : vitesse += niveau;
				break;
			case 4 : nbActions += niveau;
				break;
		}
	}

	/**
	 * Permet de gagner un niveau, lors d'un gain de niveau, une nouvelle compétence est allouée au Professeur
	 */
	private void gagnerNiveau() {
		++niveau;
		gagnerCompetence();
	}

	/**
	 * Ajoute les nouveaux points d'expérience et recalcule le niveau du Professeur
	 * @param points correspond aux points d'xp gagnés
	 */
	public void gagnerXP(int points) {
		xp += points;
		if (xp > 4) {
			gagnerNiveau();
			xp = xp - 5;
		} //Tous les 5 points d'xp, le niveau du Professeur augmente

	}

	/**
	 * Retourne le nom des équipements contenus dans l'inventaire, sous forme d'une chaîne de caractères
	 * @return un String contenant les noms des équipements de l'inventaire
	 */
	public String[] getInventaire() {
		String[] stringInventaire = new String[inventaire.size()];
		int i = 0;
		for (Equipement e : inventaire) {
			stringInventaire[i] = e.getNom();
			i++;
		}
		return stringInventaire;
	}

	/**
	 *	Permet de définir l'équipement courant à partir du nom de l'équipement
	 * @param equip correspond au nom de l'équipement à définir en tant qu'équipement courant
	 */
	public void setEquipementCourant(String equip) {
		if (equip != null) {
			int i = 0;

			while (!inventaire.get(i).nomIdentique(equip) && i < inventaire.size()) {
				i++;
			}
			if (inventaire.get(i).nomIdentique(equip)) {
				equipementCourant = inventaire.get(i);
			}

		} else {
			equipementCourant = null;
		}
	}

	/**
	 * Permet d'enlever aléatoirement un équipement de l'inventaire
	 */
	public String enleverEquipement() {
		if (!inventaire.isEmpty()) {
			int rand = (int) (Math.random() * (inventaire.size() - 1));

			String equipementPerdu = inventaire.get(rand).getNom();

			inventaire.remove(rand);
			return equipementPerdu + " a été retiré de l'inventaire.\n";
		}
		else {
			return "Aucun équipement dans l'inventaire.\n";
		}

	}

	/**
	 * Permet de savoir si un étudiant est associé
	 * @return vrai si un étudiant est associé, faux sinon
	 */
	public boolean aEtudiantAssocie() {
		return (this.etudiant != null);
	}

	/**
	 * Permet de diminuer les pv du professeur, et de modifier son statut si les pv <= 0
	 * @param pv correspond aux pv à retirer à la jauge de santé du professeur
	 */
	@Override
	public void diminuerPV(int pv) {
		super.diminuerPV(pv);
		if (this.pv <= 0) {
			enBurnOut = true;
			if (indicePosteOccupe != -1) { //Si le professeur était sur un poste, il libère le poste
				((Salle)(zonePosition)).libererPoste(indicePosteOccupe);
				//Il faut replacer l'étudiant dans le couloir
				int nouvellePosition = zonePosition.getIndiceZone() - Jeu.nbrColonnes;
				etudiant.setZonePosition(plateau.getZone(nouvellePosition));
				System.out.println("L'étudiant a été replacé dans le couloir.");

			}

			if (etudiant != null) { //Si étudiant associé, on désassocie le professeur de l'étudiant
				etudiant.setProfesseur(null);
				etudiant = null;
				System.out.println("L'étudiant a été désassocié du professeur.");
			}

			zonePosition.retirerProfesseur(this);
			System.out.println("Le professeur a été retiré de sa zone");

		}

	}

	/**
	 * Permet de savoir si le professeur n'a plus de pv
	 * @return
	 */
	public boolean enBurnout() {
		return enBurnOut;
	}

	public int getNiveau() {
		return niveau;
	}

	public int getXp() {
		return xp;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	@Override
	public String toString() {
		return nom;
	}

	public boolean isInstallerAPoste() {
		return installerAPoste;
	}

	public Color getCouleurPion() {
		return couleurPion;
	}

	public int getIndicePosteOccupe() {
		return indicePosteOccupe;
	}
}