package Models;

public abstract class Personne {

	protected String nom;
	protected int pv;
	protected int puissance;
	protected int vitesse;
	protected int nbActions;
	protected int compteurActions = 0;
	protected Zone zonePosition;

	//Les Personnes, Professeurs comme Etudiants, doivent pouvoir accéder au Plateau afin d'effectuer leurs actions
	protected Plateau plateau;

	/**
	 * Permet de diminuer les pv de la personne, que ce soit un professeur ou un étudiant
	 * @param pv correspond aux pv à retirer à la jauge de santé de la personne
	 */
	public void diminuerPV(int pv) {
		this.pv -= pv;
		System.out.println(nom + " a perdu " + pv + " points de vie");
	}

	public void neRienFaire(){
		System.out.println(nom + " ne fait rien... Pour le moment !");
	}

	public int getPv() {
		return pv;
	}

	public int getNbActions() {
		return nbActions;
	}

	public int getCompteurActions() {
		return compteurActions;
	}

	public void setCompteurActions(int compteurActions) {
		this.compteurActions = compteurActions;
	}

	public void setZonePosition(Zone zonePosition) {
		this.zonePosition = zonePosition;
	}

	public Zone getZonePosition() {
		return zonePosition;
	}

	public String getNom() {
		return nom;
	}
}
