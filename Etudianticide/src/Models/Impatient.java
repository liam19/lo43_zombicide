package Models;

public class Impatient extends Etudiant {

    /**
     * Constructeur par défaut
     */
    public Impatient(){
        super();
        initialiseImpatient();
    }


    public Impatient(int zone, Plateau plateau){
        super(zone,plateau);
        initialiseImpatient();
    }

    /**
     * Initialise les stats de Porteur de Problèmes à ses valeurs qui diffèrent
     */
    public void initialiseImpatient() {
        this.vitesse = 2;
    }

    @Override
    public String toString() {
        return super.toString() + "impatient(e)";
    }
}
