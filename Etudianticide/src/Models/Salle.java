package Models;

import java.util.ArrayList;

public class Salle extends Zone {

	private ArrayList<Poste> postes;

	public Salle(int indiceSalle, int nbPostes){
		super(indiceSalle);
		postes = new ArrayList<>();
		initialiserSalle(nbPostes);
	}

	/**
	 * Méthode permettant d'initialiser les postes dans la salle
	 * @param nbPostes correspond au nombre de postes à instancier
	 */
	public void initialiserSalle(int nbPostes) {
		for (int i = 1; i <= nbPostes; i++) {
			postes.add(new Poste(i));
		}
	}


	/**
	 * Permet d'occuper un poste, c'est-à-dire de lui associer un professeur et un étudiant
	 * @param index correspond à l'indice du poste à occuper
	 * @param professeur correspond au professeur à associer
	 * @param etudiant correspond à l'étudiant à associer
	 * @return la valeur de l'indice du poste occupé
	 */
	public int occuperPoste(int index, Professeur professeur, Etudiant etudiant) {
		postes.get(index).setProfesseur(professeur);
		postes.get(index).setEtudiant(etudiant);
		postes.get(index).setDisponible(false);

		System.out.println("On occupe le poste n°" + index);

		return postes.get(index).getIndice(); //On retourne le poste au professeur
	}

	/**
	 * Méthode permettant de savoir si le poste indiqué par l'indice est disponible
	 * @param indicePoste correspond à l'indice du poste
	 * @return un booléen : vrai si le poste est disponible, faux sinon
	 */
	public boolean posteEstDisponible(int indicePoste) {
		if (indicePoste < postes.size() && indicePoste >= 0) {
			return postes.get(indicePoste).estDisponible();
		}
		else {
			return false;
		}
	}

	/**
	 * Méthode permettant de libérer un poste (= réinitialiser les attributs du poste)
	 * @param index correspond à l'indice du poste à libérer
	 * @return une valeur par défaut : -1
	 */
	public int libererPoste(int index) {
		postes.get(index).initialiserPoste(); //Réinitialise les attributs du poste

		System.out.println("On libère le poste n°" + index);

		return -1;
	}

}
