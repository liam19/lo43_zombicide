package Models;

public class Poste {

	private boolean disponible;
	private int indice;
	private Etudiant etudiant;
	private Professeur professeur;
	
	public Poste(){
		initialiserPoste();
		indice = -1;
	}

	public Poste(int indice) {
		initialiserPoste();
		this.indice = indice;
	}

	public void initialiserPoste() {
		disponible = true;
		etudiant = null;
		professeur = null ;
	}


	public boolean estDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}

	public Professeur getProfesseur() {
		return professeur;
	}

	public int getIndice() {
		return indice;
	}
}
