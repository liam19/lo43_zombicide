package Models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Attention à la présence du fichier "equipements.txt" !
 */

public class Equipement {

	private String nom;

	private int taux_reussite;

	private int degat;

	public Equipement() {
		String[] equipement = genererEquipement(); //Appel de la fonction genererEquipement() pour récupérer une ligne parsée du .txt
		this.nom = equipement[0];
		this.taux_reussite = Integer.parseInt(equipement[1]);
		this.degat = Integer.parseInt(equipement[2]);
	}

	/**
	 * Redéfinition de la méthode toString
	 * @return
	 */
	public String toString() { 
		return nom + " | Taux de reussite : " + taux_reussite + " | Degats : " + degat;
	}


	/**
	 * Cette méthode permet de récupérer le nom, le taux de réussite ainsi que les dégâts
	 * d'un équipement ALEATOIREMENT CHOISI
	 * @return un tableau de chaînes de caractères, composé du nom, du taux de réussite et des dégâts
	 */
	private String[] genererEquipement() {
		//On utilise la méthode "split" afin de diviser notre String en tableau de String
		String[] decompose = new String[3];
		LecteurFichier lecteur = new LecteurFichier("Resources/equipements.txt");
		int nombreAleatoire = (int) (Math.random() * lecteur.getLignesFichier().size());

		decompose = lecteur.getLignesFichier().get(nombreAleatoire).split("-");

		return decompose;

	}


	/**
	 * Permet de vérifier si le nom de l'équipement est égale à la chaîne de caractères passées en paramètres
	 * @param nom correspond au String à comparer avec le nom de l'équipement
	 * @return un booléen, vrai si le le nom est identique, faux sinon
	 */
	public boolean nomIdentique(String nom) {
		return this.nom.equals(nom);
	}


	/**
	 * Implémentation des Getters
	 * @return les valeurs des attributs
	 */

	public String getNom() {
		return nom;
	}

	public int getDegat() {
		return degat;
	}

	public int getTaux_reussite() {
		return taux_reussite;
	}

}
