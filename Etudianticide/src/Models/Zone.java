package Models;

import java.util.ArrayList;

public abstract class Zone {

	protected int indiceZone;
	protected ArrayList<Professeur> professeurs;
	protected ArrayList<Etudiant> etudiants;

	public Zone(int indiceZone) {
		professeurs = new ArrayList<>();
		etudiants = new ArrayList<>();
		this.indiceZone = indiceZone;
	}

	public void ajouterProfesseur(Professeur professeur) {
		professeurs.add(professeur);
	}

	public void ajouterEtudiant(Etudiant etudiant) {
		etudiants.add(etudiant);
	}

	public void retirerProfesseur(Professeur professeur) {
		professeurs.remove(professeur);
	}

	public void retirerEtudiant(Etudiant etudiant) {
		etudiants.remove(etudiant);
	}

	public Etudiant recupererEtudiant(int indiceEtudiant) {
		return etudiants.get(indiceEtudiant);
	}

	public Professeur recupererProfesseur(int indiceProfesseur) {
		return professeurs.get(indiceProfesseur);
	}

	public boolean contientProfesseurs() {
		return !professeurs.isEmpty();
	}

	public int getIndiceZone() {
		return indiceZone;
	}

	public ArrayList<Etudiant> getEtudiants() {
		return etudiants;
	}

	/**
	 * Permet de récupérer les noms de tous les étudiants DISPONIBLES dans la zone
	 * @return un tableau de String avec tous les noms des étudiants disponibles
	 * @throws NullPointerException si etudiant ou etudiantsDispo sont vides
	 */
	public String[] recupererNomsEtudiants() throws NullPointerException {
		if (etudiants.size() > 0) {
			//On commence déjà par récupérer tous les étudiants disponibles, donc non attribués
			ArrayList<Etudiant> etudiantsDispo = new ArrayList<>();
			for (Etudiant etudiant :
					etudiants) {
				if (!etudiant.aProfAssocie()) {
					etudiantsDispo.add(etudiant);
				}
			}

			if (etudiantsDispo.size() > 0) {
				String[] noms = new String[etudiantsDispo.size()];
				int i = 0;
				for (Etudiant etudiant :
						etudiantsDispo) {
					noms[i] = etudiant.toString();
					i++;
				}

				return noms;
			}
		}
		throw new NullPointerException();
	}

	public boolean contientEtudiants() {
		return !etudiants.isEmpty();
	}

}
