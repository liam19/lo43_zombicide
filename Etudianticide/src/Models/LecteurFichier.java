package Models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LecteurFichier {
    private String chemin;
    private ArrayList<String> lignesFichier;

    public LecteurFichier(String chemin) {
        this.chemin = chemin;
        this.lignesFichier = new ArrayList<>();
        initialiserLecteur();
    }

    private void initialiserLecteur() {
        try {
            FileReader c = new FileReader(chemin); // On ouvre notre fichier / à modifier en cas de changement de répertoire
            BufferedReader r = new BufferedReader(c); // On initialise un buffer afin de lire le contenu du fichier

            String line;

            while ((line = r.readLine()) != null) {
                lignesFichier.add(line);
            }

            lignesFichier.remove(0);

            //On ferme notre buffer
            r.close();
        } catch (FileNotFoundException e) { // Si le fichier n'a pas pu être ouvert...
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getLignesFichier() {
        return lignesFichier;
    }
}
