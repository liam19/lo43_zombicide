package Models;

public abstract class Etudiant extends Personne {

	//Pas sûr que ce soit nécessaire
	//private int salleCible;

	protected Professeur professeur; //On associe un Professeur à l'Etudiant lors de l'action recontrerEtudiant du Professeur

	public Etudiant() {
		initialiserEtudiant();
		this.zonePosition = null;
	}

	public Etudiant(int zone, Plateau plateau) {
		this();
		this.plateau = plateau;
		this.zonePosition = plateau.getZone(zone);
		this.zonePosition.ajouterEtudiant(this);
	}

	/**
	 * On initialise les valeurs par défaut des attributs
	 */
	private void initialiserEtudiant() {
		this.pv = 1;
		this.nom=genererNom();
		this.nbActions = 1;
		this.puissance = 1;
		this.vitesse = 1;
		this.professeur = null;
	}

	/**
	 * Cette méthode permet de récupérer le nom d'un étudiant ALEATOIREMENT
	 * @return le nom de l'étudiant, sous forme d'un String
	 */
	protected String genererNom() {
		LecteurFichier lecteur = new LecteurFichier("Resources/nom_etudiant.txt");
		int nombreAleatoire = (int) (Math.random() * lecteur.getLignesFichier().size());
		return lecteur.getLignesFichier().get(nombreAleatoire);
	}

	/**
	 * Attaque de l'étudiant
	 * Si un Professeur est associé à l'Etudiant, celui ci l'attaquera en priorité
	 * Sinon, l'Etudiant attaque un Professeur au hasard dans la zone
	 */
	public String casserLesNoix() {
		String equipPerdu;
		String pvPerdu;
		Professeur profAttaque;

		if (professeur == null) { //Si aucun Professeur attribué, on sélectionne un Professeur au hasard dans la Zone
			int randProf = (int)(Math.random() * zonePosition.professeurs.size());
			profAttaque = zonePosition.recupererProfesseur(randProf);

		} else { //Sinon, on sélectionne le Professeur attribué
			profAttaque = this.professeur;
		}

		profAttaque.diminuerPV(this.puissance); //On attaque
		pvPerdu = profAttaque.getNom() + " a perdu " + this.puissance + " points de vie.\n";
		equipPerdu = profAttaque.enleverEquipement(); //On enlève un équipement au hasard

		return pvPerdu + equipPerdu;
	}

	/**
	 * Méthode permettant à un étudiant de se déplacer aléatoirement dans les couloirs
	 * @return un String contenant le déplacement effectué
	 */
	public String seDeplacer() {
			if (!(zonePosition instanceof Salle)) {
				int position = zonePosition.getIndiceZone();
				int col = (zonePosition.getIndiceZone() % (Jeu.nbrColonnes + 1)); //On obtient l'indice de la colonne

				zonePosition.retirerEtudiant(this);//On commence par retirer l'étudiant de la Zone actuelle

				if (col == 1) {
					zonePosition = plateau.getZone(position + 1);
				} else if (col == Jeu.nbrColonnes) {
					zonePosition = plateau.getZone(position - 1);
				} else {
					int deplacementRandom = (int) (Math.random());
					switch (deplacementRandom) {
						case 0:
							zonePosition = plateau.getZone(position + 1);
							break;
						case 1:
							zonePosition = plateau.getZone(position - 1);
							break;
					}
				}
				zonePosition.ajouterEtudiant(this); //Après déplacement, on ajoute l'Etudiant à la nouvelle Zone
				return "L'étudiant " + nom + " s'est déplacé(e) ! Position actuelle : " + zonePosition.getIndiceZone();
			}
		return "";
	}

	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}

	public Professeur getProfesseur() {
		return professeur;
	}

	@Override
	public String toString() {
		return this.nom + ", un(e) étudiant(e) ";
	}

	/**
	 * Permet de savoir si un étudiant est associé à un professeur
	 * @return un booléen : vrai si l'étudiant est associé à un professeur, faux sinon
	 */
	public boolean aProfAssocie() {
		return professeur != null;
	}

	/**
	 * Méthode reprenant la méthode du même nom de la classe Personne, permettant de diminuer les PV d'un étudiant,
	 * et de retirer l'étudiant du jeu si ses PV atteignent 0
	 * @param pv correspond aux pv à retirer à la jauge de santé de la personne
	 */
	@Override
	public void diminuerPV(int pv) {
		super.diminuerPV(pv);
		if (this.pv <= 0) {
			zonePosition.retirerEtudiant(this); //On enlève l'étudiant de sa zone
			Jeu.etudiants.remove(this); //On enlève l'étudiant du jeu
			professeur = null;
			System.out.println("Les UVs de l'étudiant(e) " + nom + " ont été validées ! " + nom + " est enlevé(e) du jeu.");
		}
	}
}
