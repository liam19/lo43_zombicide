package Models;

import java.awt.*;
import java.util.ArrayList;

public class Jeu {

    public static ArrayList<Professeur> professeurs;
    public static ArrayList<Etudiant> etudiants;
    public static int nbrLignes = 2;
    public static int nbrColonnes = 3;
    public static int nbrCouloirs = 3;
    public static int nbrSalles = 3;
    public static int nbrPostes = 6;
    public static int tour = 1;
    public static int objectifTour = 10;

    private De de;
    private Plateau plateau;

    /**
     * Constructeur par défaut
     */
    public Jeu() {
        initialiserJeu();
    }

    /**
     * On initialise les attributs du Jeu
     */
    public void initialiserJeu() {
        plateau = new Plateau(nbrCouloirs,nbrSalles,nbrPostes);
        professeurs = new ArrayList<>();
        genereProfesseur();
        etudiants = new ArrayList<>();
        de = new De();
    }

    /**
     * Permet de vérifier si la partie est gagnée ou perdue - met à jour victoire ou défaite
     */
    public String verificationFinTour() {
        StringBuilder result = new StringBuilder();
        ArrayList<Professeur> profASupprimer = new ArrayList<>();
        //On commence par vérifier si les Professeurs sont "en burnout" (=KO), et les stocker dans profASupprimer
        for (Professeur prof : professeurs) {
            if (prof.enBurnout()) { //Si c'est le cas, on les retire de la liste professeurs
                result.append(prof.getNom() + "-");
                profASupprimer.add(prof);
            }
        }
        //On supprime les professeurs en burn-out
        for (Professeur prof :
             profASupprimer) {
            professeurs.remove(prof);
        }

        //Si tous nos profs sont KO OU que le nombre d'étudiants dans le jeu est supérieur ou égal à 15
        if (defaite()) return "Défaite";
        if (victoire()) return "Victoire";

        ++tour; //On passe au tour suivant

        //Si pas de victoire ni de défaite, on retourne le contenu de result
        return result.toString();
    }

    private boolean defaite() {
        //Si tous nos profs sont KO OU que le nombre d'étudiants dans le jeu est supérieur ou égal à 15
        return professeurs.isEmpty() || etudiants.size() >= 15;
    }

    private boolean victoire() {
        return tour == objectifTour;
    }

    /**
     * Méthode permettant d'effectuer les tours de tous les étudiants
     * @return un String contenant toutes les informations relatives aux tours des étudiants
     */
    public String tourEtudiants() {
        StringBuilder phaseA = new StringBuilder();
        StringBuilder phaseD = new StringBuilder();
        for (Etudiant etudiant : etudiants) {
            for (int i = 0; i < etudiant.vitesse; i++) { //L'étudiant peut se déplacer plusieurs fois
                if (etudiant.getZonePosition().contientProfesseurs()) { //Si des professeurs se trouvent dans la zone, l'étudiant attaque
                    phaseA.append(etudiant.casserLesNoix()).append("\n");
                } else { //Sinon, il se déplace
                    phaseD.append(etudiant.seDeplacer()).append("\n");
                }
            }
        }

        String phaseAttaque = phaseA.toString();
        boolean isAttaque = !phaseAttaque.equals("");
        String phaseDeplacement = phaseD.toString();
        boolean isDeplacement = !phaseDeplacement.equals("");

        String actionEtudiants = "";
        if (isAttaque) {
            actionEtudiants += "Phase de cassage de noix !\n" + phaseAttaque;
        }
        if (isDeplacement) {
            actionEtudiants += "Phase de déplacement !\n" + phaseDeplacement + "\n";
        }
        return actionEtudiants + genereEtudiants(getNiveauMaxProf());
    }

    /**
     * Permet de générer un certain nombre d'étudiants, que l'on rajoute à etudiants --Fonctionnel
     * @param nbEtudiants correspond au nombre d'étudiants à ajouter à la etudiants
     * @return un String contenant les informations concernant les étudiants générés
     */
    public String genereEtudiants(int nbEtudiants) {
        int i = 0;
        StringBuilder nouveauxEtudiants = new StringBuilder();
        nouveauxEtudiants.append("Invasion !! De nouveaux étudiants sont apparus:\n");
        //Ensuite, on ajoute de nouveaux étudiants de type Préparé, Impatient ou Porteur de Problème ALEATOIREMENT
        while(i < nbEtudiants) {
            int randZone = (int)(Math.random() * 2) + 1;
            int rand = (int)(Math.random() * 3);
            switch (rand) {
                case 0 : etudiants.add(new Prepare(randZone,plateau));
                break;
                case 1 : etudiants.add(new Impatient(randZone,plateau));
                break;
                case 2 :
                    //Cette étape rend l'apparition des porteurs de problèmes plus rare
                    int randPorteurProb = (int)(Math.random() * 2);
                    if (randPorteurProb == 0) {
                        etudiants.add(new Porteur_probleme(randZone,plateau));
                    }
                    else {
                        etudiants.add(new Prepare(randZone,plateau));
                    }
                break;
            }
            nouveauxEtudiants.append(etudiants.get(etudiants.size()-1).toString()).append("\n");
            ++i;
        }
        System.out.println(nouveauxEtudiants);

        return nouveauxEtudiants.toString();
    }

    /**
     * Méthode permettant d'obtenir le plus haut niveau atteint par tous les professeurs
     * @return le niveau maximum
     */
    public int getNiveauMaxProf() {
        int max = 1;
        for (Professeur prof:
                professeurs) {
            if (prof.getNiveau() > max) {
                max = prof.getNiveau();
            }
        }
        return max;
    }

    /**
     * On génère les 6 professeurs
     */
    public void genereProfesseur() {

        professeurs.add(new Professeur("Charles", Color.BLUE,1,plateau));
        professeurs.add(new Professeur("Bernard",Color.ORANGE,1,plateau));
        professeurs.add(new Professeur("François",Color.RED,1,plateau));
        professeurs.add(new Professeur("Philippe",Color.GREEN,1,plateau));
        professeurs.add(new Professeur("Yves",Color.MAGENTA,1,plateau));
        professeurs.add(new Professeur("Jean",new Color(102,51,0),1,plateau));

        professeurs.forEach(System.out::println);
    }

    /**
     * Getters
     */
    public Plateau getPlateau() {
        return plateau;
    }

    public De getDe() {
        return de;
    }
}