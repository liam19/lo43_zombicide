package Models;

import java.util.HashMap;

public class Plateau {
	private int nbLignes; //Nombre de lignes du plateau
	private int nbColonnes; //Nombre de colonnes du plateau
	private int nbrSalles;
	private int nbrCouloirs;
	private HashMap<Integer,Zone> plateau = new HashMap<>();

	public Plateau(){
		this.nbrCouloirs=0;
		this.nbrSalles=0;
		initialiserPlateau(0);
	}

	public Plateau(int nbrCouloirs, int nbrSalles, int nbrPostes){
		this.nbrCouloirs = nbrCouloirs;
		this.nbrSalles = nbrSalles;
		initialiserPlateau(nbrPostes);
	}


    /**
     * crée une hashmap contenant n couloirs et m salles et pour chacune de ces zones est associée une clé.
     * Les premiers slots de la hashmap sont alloués pour les salles et les suivants pour les couloirs.
     */
    public void initialiserPlateau(int nbrPostes) {
    	int i;
	    for (i = 1; i <= nbrCouloirs; i++){
			plateau.put(i,new Couloir(i));
        }
        for (i = nbrCouloirs + 1; i <= nbrCouloirs + nbrSalles; i++){
			plateau.put(i,new Salle(i,nbrPostes));
        }
    }


	public Zone getZone(int indiceZone) {
		return plateau.get(indiceZone);
	}
}
