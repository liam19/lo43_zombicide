package Models;

public class De {

	private int nbFaces = 6;

    /**
     * Lance le dé pour obtenir la valeur correspondante
     * @return la valeur du lancer de dé
     */
	public int lancerDe() {

		return (int) (Math.random() * nbFaces) + 1;
	}
}
