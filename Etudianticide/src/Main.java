import Controllers.JeuController;
import Models.Jeu;

import javax.swing.*;


public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new JeuController(new Jeu()));
    }
}
